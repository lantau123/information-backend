var express = require('express');
var router = express.Router();

//引入jwt
const jwt = require('jsonwebtoken');
const { randomCode, sendCode } = require("./getMessage")

//引入数据库
const { UserModel,CollectionModel,FootprintModel,}  = require("../../models/zhh")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// //手机号验证码登录
router.get('/phone/:phone', async (req, res) => {
  let Phone = req.params.phone
  let data = await UserModel.findOne({ Phone })
  console.log(data);
  if (data) {//查询成功---用户输入的手机号存在于数据库中
      let token = "Bearer " + jwt.sign({}, "tdy", { expiresIn: '24h' })
      res.send({ code: 200, token, data })
  } else {//查询失败 --没有查询到用户输入的手机号
      res.send({ code: 400, message: '手机号未注册' })
  }
})

//发送验证码
router.get("/captcha", async (req, res) => {
  //将我们封装好的方法在此进行调用 生成一个四位数的验证码
  let captcha = randomCode(4)
  // 将发送验证码的方法在此调用 这里的第一个参数就是发送验证码的手机号 这个可以从用户输入的信息获取 第二个参数是发送的验证码 第三个参数是一个回调函数 用来判断是否发送成功
  sendCode('18031015146', captcha, function (success) {
    if (success) {
      let token = "Bearer " + jwt.sign({}, "tdy", { expiresIn: '24h' })
      res.send({
        code: 200,
        msg: "发送成功",
        token,
        captcha
      })
    } else {
      res.send({ code: 400, message: '验证码发送失败' })
    }
  })
  let token = "Bearer " + jwt.sign({}, "secret", { expiresIn: '24h' })
  res.send({
      code: 200,
      msg: "发送成功",
      token,
      captcha
  })
})

module.exports = router;

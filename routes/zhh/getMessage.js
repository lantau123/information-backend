var md5 = require('blueimp-md5')
var moment = require('moment')
var Base64 = require('js-base64').Base64;
var request = require('request');
//
/*这几行代码引入了需要的第三方库，分别是 blueimp-md5 用于进行MD5加密，moment 用于处理日期和时间，
//js-base64 用于进行Base64编码和解码，以及 request 用于发送HTTP请求。
 生成指定长度的随机数
 */
function randomCode(length) {
    var chars = ['0','1','2','3','4','5','6','7','8','9'];
    var result = ""; //统一改名: alt + shift + R
    for(var i = 0; i < length ; i ++) {
        var index = Math.ceil(Math.random()*9);//在循环体内部，首先通过 Math.random() 函数生成一个0到1之间的随机数，然后乘以9，
        //结果范围为0到9之间的浮点数。接着，使用 Math.ceil() 函数向上取整，将浮点数转换为整数，
        //范围为1到9之间的整数。这个整数 index 用于从字符数组 chars 中选择对应的字符。
        result += chars[index];
    }
    return result;
}
// console.log(randomCode(6));
//这是一个函数 randomCode，用于生成指定长度的随机数验证码。它通过在字符数组 chars 中随机选择字符
//，并将选择的字符拼接到结果字符串 result 中，最后返回结果字符串
/*
向指定号码发送指定验证码
 */
function sendCode(phone, code, callback) {//这是发送短信验证码的函数 sendCode。它接受三个参数：
    //phone 表示要发送验证码的手机号码，code 表示验证码内容，callback 表示回调函数，在发送短信后执行。
    
    var ACCOUNT_SID = '2c94811c89e22612018a27580d110efa';
    var AUTH_TOKEN = '66a61cc7cc344914ac4a8cf26582b153';
    var Rest_URL = 'https://app.cloopen.com:8883';
    var AppID = '2c94811c89e22612018a27580e6b0f01';
    //在函数内部，首先定义了一些常量，包括 ACCOUNT_SID（账户ID）、
    //AUTH_TOKEN（账户授权令牌）、Rest_URL（请求的URL地址）和 AppID（应用ID
    //1. 准备请求url
    /*
     1.使用MD5加密（账户Id + 账户授权令牌 + 时间戳）。其中账户Id和账户授权令牌根据url的验证级别对应主账户。
     时间戳是当前系统时间，格式"yyyyMMddHHmmss"。时间戳有效时间为24小时，如：20140416142030
     2.SigParameter参数需要大写，如不能写成sig=abcdefg而应该写成sig=ABCDEFG
     */
    var sigParameter = '';
    var time = moment().format('20230616103750');
    sigParameter = md5(ACCOUNT_SID+AUTH_TOKEN+time);
    var url = Rest_URL+'/2013-12-26/Accounts/'+ACCOUNT_SID+'/SMS/TemplateSMS?sig='+sigParameter;//然后，根据规定的格式生成时间戳 time，并使用 md5 函数对 ACCOUNT_SID+AUTH_TOKEN+time 进行加密，
    //得到 sigParameter，用于构造请求的URL

    //2. 准备请求体
    var body = {
        to : phone,
        appId : AppID,
        templateId : '1',
        "datas":[code,"1"]
    }
    // body = JSON.stringify(body);
    //接下来，构造请求体 body，包含了手机号码、应用ID、模板ID和数据数组。
    //其中，code 是要发送的验证码，"1" 表示模板中的一个参数
    console.log(body);


    //3. 准备请求头
    /*
     1.使用Base64编码（账户Id + 冒号 + 时间戳）其中账户Id根据url的验证级别对应主账户
     2.冒号为英文冒号
     3.时间戳是当前系统时间，格式"yyyyMMddHHmmss"，需与SigParameter中时间戳相同。
     */
    var authorization = ACCOUNT_SID + ':' + time;
    //生成请求头 headers，包括了接受的数据类型、内容类型、内容长度和授权信息
    authorization = Base64.encode(authorization);
    var headers = {
        'Accept' :'application/json',
        'Content-Type' :'application/json;charset=utf-8',
        'Content-Length': JSON.stringify(body).length+'',
        'Authorization' : authorization
    }

    //4. 发送请求, 并得到返回的结果, 调用callback
	  // callback(true);
      //最后，使用 request 发送 POST 请求到指定的 URL，将请求头和请求体作为参数传递，
      //并在回调函数中处理请求的响应结果。在回调函数中，首先打印错误信息、响应信息和响应体，
      //然后根据响应体的状态码判断发送是否成功，最后调用回调函数 callback 并传递发送状态
    request({
        method : 'POST',
        url : url,
        headers : headers,
        body : body,
        json : true
    }, function (error, response, body) {
        console.log(error,response);
        console.log(body);
        callback(body.statusCode==='000000');
    });
}
module.exports = {sendCode,randomCode}

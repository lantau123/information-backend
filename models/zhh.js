const { use } = require('../routes/zhh')
const mongoose = require('./db')
const Schema = mongoose.Schema

//用户
let UserSchema = new Schema({
    UserName:String,
    PassWord:String,
    Phone:Number,
    PaymentPwd:Number,
    Price:Number,
    collectionId:{
        type:mongoose.Types.ObjectId, //与收藏页面建立外键联系
        ref:'collection'
    },
    footprintId:{
        type:mongoose.Types.ObjectId, //与足迹页面建立外键联系
        ref:'footprint'
    }
})

//收藏
let CollectionSchema = new Schema({
    title:String,
    num:Number,
    price:Number,
    discount:Number,
    img:String
})

//足迹
let FootprintSchema = new Schema({
    img:String,
    path:String
})

//订单
let OrdersSchema = new Schema({
    state:Number //state有四个状态 0：待接单 1：已接单 2回收中 3：已完成
})

let UserModel = mongoose.model('user',UserSchema,'user')
let CollectionModel = mongoose.model('collection',CollectionSchema,'collection')
let FootprintModel = mongoose.model('footprint',FootprintSchema,'footprint')

// UserModel.create({
//     UserName:'admin',
//     PassWord:'123456',
//     Phone:18031015146,
//     PaymentPwd:123456,
//     Price:1564,
// })

module.exports = {
    UserModel,
    CollectionModel,
    FootprintModel,
}
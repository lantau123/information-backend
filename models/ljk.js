const mongoose = require('./db')
const Schema = mongoose.Schema


const storeSchema = new Schema({
    SUname:String,
    storeName:String,
    image:String,
    address:String,
    tags:Array,
    distance:Number,
    phone:String,
    lng:Number,
    lat:Number
})

const storeModel = mongoose.model('store',storeSchema,'store')

// storeModel.create({
//     SUname:'某先生',
//     storeName:"OMINI来福士XXX",
//     image:"",
//     address:"保定市莲池区XXXX",
//     tags:['口碑','营业中'],
//     distance:0,
//     phone:"010-6589250009",
//     lng:115.46981799999999,
//     lat:38.8738
// })


module.exports = {
    storeModel
}
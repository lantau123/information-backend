const mongoose = require('./db')

const shujuSchema = new mongoose.Schema({
    jinbi: String,
    img: String,
    jinbinum: String
})
const remenSchema = new mongoose.Schema({
    jinbi: String,
    img: String,
    jinbinum: String
})

const xihuanSchema = new mongoose.Schema({
    title: String,
    img: String,
    price: String,
    xiangqing: String,
    yuanprice: String,
    imgss: Array
})

const datalist = mongoose.model('onelist', shujuSchema, 'onelist')
const remendata = mongoose.model('relist', remenSchema, 'relist')
const xihuandata = mongoose.model('xihuan', xihuanSchema, 'xihuan')

// datalist.create({
//     jinbi: "100",
//     img: "https://img.axureshop.com/8d/2f/30/8d2f3075c60b454382a8e5b016e47cf6/images/%E9%A6%96%E9%A1%B5/%E5%95%86%E5%93%81%E5%9B%BE_u158-0.png",
//     jinbinum: "125"
// })
// remendata.create({
//     jinbi: "100",
//     img: "https://img.axureshop.com/8d/2f/30/8d2f3075c60b454382a8e5b016e47cf6/images/%E9%A6%96%E9%A1%B5/%E5%95%86%E5%93%81%E5%9B%BE_u158-0.png",
//     jinbinum: "125"
// })
xihuandata.create({
    title: "夏季必备风扇",
    img: "https://img.axureshop.com/8d/2f/30/8d2f3075c60b454382a8e5b016e47cf6/images/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85/u3985.png",
    price: "125",
    xiangqing: "创意卫浴五件套 欧式高档卫生间洗漱杯套装浴室用品刷牙杯漱口杯",
    yuanprice: "300",
    imgss: [
        "https://img.axureshop.com/8d/2f/30/8d2f3075c60b454382a8e5b016e47cf6/images/%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85/u3985.png",
        "https://img.axureshop.com/8d/2f/30/8d2f3075c60b454382a8e5b016e47cf6/images/%E5%85%91%E6%8D%A2%E5%95%86%E5%9F%8E%E9%A6%96%E9%A1%B5/pic_u3613-2.png"
    ]
})

module.exports = {
    datalist,
    remendata,
    xihuandata
}
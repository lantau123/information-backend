var express = require('express');
var router = express.Router();
var {storeModel}  = require('../../models/ljk')

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });


router.get('/getStoreList',async(req,res)=>{
  let data = await storeModel.find({})
  console.log(data)
  res.send({
    code:200,
    msg:'成功',
    data
  })
})

module.exports = router;

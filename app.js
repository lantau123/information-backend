var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cors = require('cors')
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var zhhRouter = require('./routes/zhh') //张浩豪
var wzhRouter = require('./routes/wzh') //王子航
var ljkRouter = require('./routes/ljk') //李继凯
var zrRouter = require('./routes/zr') //赵然
var yzkRouter = require('./routes/yzk')//于泽康
var yrtRouter = require('./routes/yrt') // 于瑞涛

var expressJWT = require('express-jwt');

var app = express();
app.use('/upload',express.static('upload'))
app.use(cors())

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// jwt鉴权
// app.use(expressJWT({
//   // 加密方式 密钥名称
//   secret : 'tdy',
//   // 加密进行转换
//   algorithms:["HS256"],
//   // 指定哪一些接口不需要通权限
// }).unless({path:['/login','/enroll','/zhh/captcha']}))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/zhh',zhhRouter) //张浩豪
app.use('/wzh',wzhRouter) //王子航
app.use('/ljk',ljkRouter) //李继凯
app.use('/zr',zrRouter) //赵然
app.use('/yzk',yzkRouter) //于泽康
app.use('/yrt',yrtRouter) //于瑞涛

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

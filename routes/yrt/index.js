var express = require('express');
var router = express.Router();
const { datalist, remendata, xihuandata } = require('../../models/yrt')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

// 兑换数据展示
router.get('/datalist', async (req, res) => {
  const data = await datalist.find()
  res.send({
    msg: "数据发送成功",
    code: 200,
    data: data
  })
})

// 热门数据展示
router.get('/relist', async (req, res) => {
  const relist = await remendata.find()

  res.send({
    code: 200,
    msg: "数据发送成功",
    data: relist
  })
})

// 猜你喜欢数据展示
router.get('/xihuanlist', async (req, res) => {
  const xilist = await xihuandata.find()

  res.send({
    code: 200,
    msg: "数据发送成功",
    data: xilist,
  })
})


module.exports = router;
